const version = "0.3.2";

const moment = require('moment'),
      _ = require('lodash');

function codeWithLength (min, max) {
  return {
    type: 'string',
    validator: v => {
      return v.length >= min && v.length <= max;
    },
    converter: v => {
      return v.toUpperCase();
    }
  };
}
module.exports = {
  'version': {
    type: 'string'
  },
  'id': { // db id
    type: 'number',
    default: -1
  },
  'datetime': {
    type: 'string',
    default: () => moment().utc().format('YYYY-MM-DD HH:mm:ss'),
    validator: v => {
      return moment(v, 'YYYY-MM-DD HH:mm:ss', true).isValid();
    }
  },
  'datetime_zone': {
    type: 'string',
    default: () => moment().utc().format('YYYY-MM-DD HH:mm:ss Z'),
    validator: v => {
      return moment(v, 'YYYY-MM-DD HH:mm:ss Z', true).isValid();
    }
  },
  'datetime_full': {
    type: 'string',
    default: () => moment().utc().format('YYYY-MM-DD HH:mm:ss.SSS Z'),
    validator: v => {
      return moment(v, 'YYYY-MM-DD HH:mm:ss.SSS Z', true).isValid();
    }
  },
  'price': {
    type: 'number'
  },
  'ratio': {
    type: 'number',
    validator: v => v <= 1
  },
  'exchanges/symbol' : codeWithLength(2, 20),
  'exchanges/name' : codeWithLength(2, 100),
  'markets/symbol' : codeWithLength(2, 10),
  'markets/name': codeWithLength(2, 20),
  'coins/symbol': { alias: 'markets/symbol' },
  'coins/name': { alias: 'markets/name' },
  'auth/id': {
    type: 'string',
    validator: v => {
      return v.length >= 4 && v.length <= 28;
    },
    converter: v => {
      return v.toLowerCase();
    }
  },
  'auth/pw': {
    type: 'string',
    validator: v => {
      return v.length >= 8 && v.length <= 100;
    },
  },
  'auth/email': {
    type: 'string',
    validator: (v, _v) => {
      return v.length >= 6 && v.length <= 100 && _v.email(v);
    },
    converter: v => {
      return v.toLowerCase();
    }
  },
  'auth/invitation_code': {
    type: 'string',
    validtor: v => {
      return v.length >= 20;
    }
  },
  /*
   * order
   */
  'order/version': {
    type: 'string',
    default: '1',
  },
  'order/id': { alias: 'id' },
  'order/user_id': { alias: 'auth/id' },
  'order/title': {
    type: 'string',
    default: ''
  },
  'order/desc': {
    type: 'string',
    default: ''
  },
  'order/author': {
    type: 'string',
    default: ''
  },
  'order/source_id': { alias: 'id' },
  'order/exchange': { type: 'string' },
  'order/market': { type: 'string' },
  'order/coin': { type: 'string' },
  'order/datetime': { alias: 'datetime_full' },
  'order/state': {
    type: 'string',
    default: 'inactive',
    validator: v => {
      return v == 'active' || v == 'inactive' || v == 'done';
    }
  },
  'order/status': {
    type: 'string',
    default: 'stuck',
    validator: v => {
      return v == 'stuck' || v == 'running' || v == 'error';
    }
  },
  'order/running_state': {
    type: 'string',
    default: 'profit',
    validator: v => {
      return v == 'losscut' || v == 'profit';
    }
  },
  'order/tradings': {
    type: 'array'
  },
  'order/entry/strategy': { // 중요
    type: ['array', 'object'],
    validator: function (v) {
      let self = this;
      if (_.isPlainObject(v)) {
        return self.resolve('order/tradings', v.tradings) &&
          typeof v.price === 'number' &&
          self.resolve('ratio', v.ratio);
      } else {
        return v.every(elem => {
          return self.resolve('order/tradings', elem.tradings) &&
            typeof elem.price === 'number' &&
            self.resolve('ratio', elem.ratio);
        });
      }
    }
  },
  'order/entry': {
    type: 'object',
    validator: function (v) {
      return typeof v.quantity === 'number' &&
        this.resolve('order/entry/strategy', v.strategy);
    }
  },
  'order/target/strategy': { alias: 'order/entry/strategy' },
  'order/target': {
    type: 'object',
    validator: function (v) {
      return this.resolve('order/entry/strategy', v.strategy);
    }
  },
  'order/stoploss': {
    type: 'object',
    validator: function (v) {
      let type = v.type,
          typeFlag = typeof type === 'number' && (type == 0 || type == 1 || type == 2);
      if (!typeFlag) return false;

      if (type == 0) return true;

      return this.resolve(`order/stoploss/strategy/${type}`, v.strategy);
    }
  },
  'order/stoploss/strategy/1': { // stop-limit
    type: 'object',
    validator: function (v) {
      return this.resolve('order/tradings', v.tradings) &&
        this.resolve('ratio', v.ratio) &&
        typeof v.stop === 'number' &&
        typeof v.limit === 'number';
    }
  },
  'order/stoploss/strategy/2': { // trailing-stop
    type: 'object',
    validator: function (v) {
      return this.resolve('order/tradings', v.tradings) &&
        this.resolve('ratio', v.ratio) &&
        this.resolve('ratio', v.stopRatio);
    }
  },
  'order': {
    type: 'object',
    validator: function (v) {
      this.resolve('order/version', v.version);
      this.resolve('order/id', v.id);
      this.resolve('order/user_id', v.user_id);
      this.resolve('order/title', v.title);
      this.resolve('order/desc', v.desc);
      this.resolve('order/author', v.author);
      this.resolve('order/source_id', v.source_id);
      this.resolve('order/exchange', v.exchange);
      this.resolve('order/market', v.market);
      this.resolve('order/coin', v.coin);
      this.resolve('order/datetime', v.created_at);
      this.resolve('order/datetime', v.updated_at);
      this.resolve('order/datetime', v.finished_at);
      this.resolve('order/state', v.state);
      this.resolve('order/status', v.status);
      this.resolve('trading/running_state', v.running_state);
      this.resolve('order/entry', v.entry);
      this.resolve('order/target', v.target);
      this.resolve('order/stoploss', v.stoploss);
    },
    converter: function (v) {
      v.order.strategy = _.orderBy(_.uniqBy(_.orderBy(v.order.strategy, o => o.ratio, ['desc']), o => o.price), o => o.price);
      v.target.strategy = _.orderBy(_.uniqBy(_.orderBy(v.target.strategy, o => o.ratio, ['desc']), o => o.price), o => o.price);
      return v;
    }
  },
  /*
   * trading
   */
  'trading/version': { alias: 'order/version' },
  'trading/id': { alias: 'id' },
  'trading/user_id': { alias: 'auth/id' },
  'trading/exchange': { type: 'string' },
  'trading/market': { type: 'string' },
  'trading/coin': { type: 'string' },
  'trading/datetime': { alias: 'datetime_full' },
  'trading/state': {
    type: 'string',
    validator: v => {
      return v == 'wait' || v == 'cancel' || v == 'done';
    }
  },
  'trading/side': {
    type: 'string',
    validator: v => {
      return v == 'bid' || v == 'ask';
    }
  },
  'trading/fee': {
    type: 'object',
    validator: function (v) {
      return typeof v.executed === 'number' &&
        typeof v.total === 'number';
    }
  },
  'trading/volume': {
    type: 'object',
    validator: function (v) {
      return typeof v.executed === 'number' &&
        typeof v.total === 'number';
    }
  },
  'trading/price': {
    type: 'object',
    validator: function (v) {
      return typeof v.asking === 'number' &&
        typeof v.executed === 'number' &&
        typeof v.locked === 'number';
    }
  },
  'trading': {
    type: 'object',
    validator: function (v) {
      this.resolve('trading/version', v.version);
      this.resolve('trading/id', v.id);
      this.resolve('trading/id', v.order_id);
      this.resolve('trading/user_id', v.user_id);
      this.resolve('trading/state', v.state);
      this.resolve('trading/side', v.side);
      this.resolve('trading/datetime', v.created_at);
      this.resolve('trading/datetime', v.checked_at);
      this.resolve('trading/datetime', v.updated_at);
      this.resolve('trading/exchange', v.exchange);
      this.resolve('trading/market', v.market);
      this.resolve('trading/coin', v.coin);
      this.resolve('trading/fee', v.fee);
      this.resolve('trading/volume', v.volume);
      this.resolve('trading/price', v.price);
      return true;
    }
  }
}
