# bitrading-validator-template

version: 0.3.2

Bitrading validator template

## Versions
### v0.3.2 (2018-11-12)
- Simplify order/tradings

### v0.3.0 (2018-11-06)
- Changed id type

### v0.2.13 (2018-11-05)
- Fix sourceId to source_id

### v0.2.12 (2018-11-05)
- Change names camelCase to underscore

### v0.2.11 (2018-11-05)
- update default values

### v0.2.10
- order/title, order/desc, order/sourceId, order/running_state 추가

### v0.2.9
- order/tradings validator 타입 변경 isPlainObject -> isObjectLike (클래스 수용가능하도록)

### v0.2.8
- Fixed tradings type

### v0.2.7
- tradingIds -> tradings 명칭 변경

### v0.2.5
- order/entry/strategy 객체타입 지원

### v0.2.4
- 버그 고침

### v0.2.3
- default 옵션 함수 적용

### v0.2.2
- default 데이터 추가

### v0.2.1
- order ,trading 타입 추가

### v0.2.0
- order, trading 관련 추가

## Types

### basic
- version
- id
- datetime
- datetime_zone
- datetime_full
- price
- ratio
- exchanges/symbol
- exchanges/name
- markets/symbol
- markets/name
- coins/symbol
- coins/name

### auth
- auth/id
- auth/pw
- auth/email
- auth/invitation_code

### order
- order/version
- order/id
- order/userId
- order/exchange
- order/market
- order/coin
- order/datetime
- order/state
- order/status
- order/tradingIds
- order/entry/strategy
- order/entry
- order/target/strategy
- order/target
- order/stoploss
- order/stoploss/strategy/1
- order/stoploss/strategy/2

### trading
- trading/version
- trading/id
- trading/userId
- trading/exchange
- trading/market
- trading/coin
- trading/datetime
- trading/state
- trading/side
- trading/fee
- trading/volume
- trading/price
